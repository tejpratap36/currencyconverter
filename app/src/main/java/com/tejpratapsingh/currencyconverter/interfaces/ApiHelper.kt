package com.tejpratapsingh.currencyconverter.interfaces

import com.tejpratapsingh.currencyconverter.data.response.ConvertResponse
import com.tejpratapsingh.currencyconverter.data.response.SymbolsResponse
import retrofit2.Response

interface ApiHelper {
    suspend fun convert(from: String, to: String, amount: Float): Response<ConvertResponse>
}