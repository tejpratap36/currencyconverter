package com.tejpratapsingh.currencyconverter.interfaces

import com.tejpratapsingh.currencyconverter.data.response.ConvertResponse
import com.tejpratapsingh.currencyconverter.data.response.SymbolsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("convert")
    suspend fun convert(@Query("from") from: String, @Query("to") toCSV: String, @Query("amount") amount: Float): Response<ConvertResponse>
}