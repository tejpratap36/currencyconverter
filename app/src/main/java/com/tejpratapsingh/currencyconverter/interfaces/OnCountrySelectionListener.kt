package com.tejpratapsingh.currencyconverter.interfaces

interface OnCountrySelectionListener {
    fun onCountrySelected()
}