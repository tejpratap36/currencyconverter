package com.tejpratapsingh.currencyconverter.data.response

data class ConvertResponse(val rate: Float, val result: Float)