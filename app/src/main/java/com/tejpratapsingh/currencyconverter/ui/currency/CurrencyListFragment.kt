package com.tejpratapsingh.currencyconverter.ui.currency

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tejpratapsingh.currencyconverter.data.response.Status
import com.tejpratapsingh.currencyconverter.databinding.FragmentCountryListBinding

class CurrencyListFragment : BottomSheetDialogFragment() {

    private var _binding: FragmentCountryListBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val currencyListViewModel by viewModels<CurrencyListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCountryListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = binding.recyclerViewCurrencyList

        currencyListViewModel.currencyList.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS) {
                recyclerView.adapter =
            } else {

            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}