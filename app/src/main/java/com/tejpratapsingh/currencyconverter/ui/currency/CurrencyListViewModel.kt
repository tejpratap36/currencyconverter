package com.tejpratapsingh.currencyconverter.ui.currency

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tejpratapsingh.currencyconverter.data.response.Resource
import com.tejpratapsingh.currencyconverter.data.response.SymbolsResponse
import com.tejpratapsingh.currencyconverter.remote.CurrencyRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class CurrencyListViewModel @Inject constructor(
    private val currencyRepository: CurrencyRepository
) : ViewModel() {

    private val _currencyList = MutableLiveData<Resource<SymbolsResponse>>().apply {
        value = Resource.loading(null)
    }
    val currencyList: LiveData<Resource<SymbolsResponse>> = _currencyList

    init {
        getSymbols()
    }

    private fun getSymbols() = viewModelScope.launch {
        _currencyList.postValue(Resource.loading(null))

        currencyRepository.getSymbols().apply {
            if (isSuccessful) {
                _currencyList.postValue(Resource.success(body()))
            } else {
                _currencyList.postValue(Resource.error(errorBody().toString(), null))
            }
        }
    }
}