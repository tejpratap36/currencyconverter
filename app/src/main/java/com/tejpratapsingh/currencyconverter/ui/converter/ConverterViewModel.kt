package com.tejpratapsingh.currencyconverter.ui.converter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ConverterViewModel : ViewModel() {

    private val _fromCurrency = MutableLiveData<String>().apply {
        value = "This is dashboard Fragment"
    }
    val fromCurrency: LiveData<String> = _fromCurrency
}