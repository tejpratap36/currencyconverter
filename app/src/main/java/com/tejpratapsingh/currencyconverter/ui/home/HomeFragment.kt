package com.tejpratapsingh.currencyconverter.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.tejpratapsingh.currencyconverter.databinding.FragmentHomeBinding
import com.tejpratapsingh.currencyconverter.ui.currency.CurrencyListFragment

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val homeViewModel by viewModels<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val buttonFromCurrency: Button = binding.buttonFromCurrency
        homeViewModel.selectedCountry.observe(viewLifecycleOwner) {
            buttonFromCurrency.text = it
        }

        buttonFromCurrency.setOnClickListener {
            CurrencyListFragment().also {
                it.show(childFragmentManager, CurrencyListFragment::class.java.simpleName)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}