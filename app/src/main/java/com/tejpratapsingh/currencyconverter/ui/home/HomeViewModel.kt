package com.tejpratapsingh.currencyconverter.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    private val _selectedCountry = MutableLiveData<String>().apply {
        value = "INR"
    }
    val selectedCountry: LiveData<String> = _selectedCountry
}