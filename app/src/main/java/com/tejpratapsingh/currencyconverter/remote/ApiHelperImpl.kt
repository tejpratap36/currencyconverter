package com.tejpratapsingh.currencyconverter.remote

import com.tejpratapsingh.currencyconverter.data.response.ConvertResponse
import com.tejpratapsingh.currencyconverter.data.response.SymbolsResponse
import com.tejpratapsingh.currencyconverter.interfaces.ApiHelper
import com.tejpratapsingh.currencyconverter.interfaces.ApiService
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(
    private val apiService: ApiService
): ApiHelper {
    override suspend fun convert(from: String, to: String, amount: Float): Response<ConvertResponse> = apiService.convert(from, to, amount)
}