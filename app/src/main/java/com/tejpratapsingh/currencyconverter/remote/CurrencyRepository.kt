package com.tejpratapsingh.currencyconverter.remote

import com.tejpratapsingh.currencyconverter.interfaces.ApiHelper
import javax.inject.Inject

class CurrencyRepository @Inject constructor(
    private val apiHelper: ApiHelper
){
    suspend fun convert(from: String, to: String, amount: Float) = apiHelper.convert(from, to, amount)
}